package day5;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class DatatableClass {
	
	WebDriver driver ;
	
	public DatatableClass(WebDriver driver) {
		this.driver = driver;
	}
	
	
	By byvarname = By.xpath("//*[@id=\"example\"]/tbody/tr[*]/td[1]");
	By byvarposition = By.xpath("//*[@id=\"example\"]/tbody/tr[*]/td[2]");
	By byvarage = By.xpath("//*[@id=\"example\"]/tbody/tr[*]/td[4]");
	
	String Compare(String name, String position, String age)
	{
		String returnValue = null;
		String Actualname = null;
		String Actualposition = null;
		String Actualage = null;
		WebElement showentrynos = driver.findElement(By.xpath("//*[@id=\"example_length\"]/label/select"));
		showentrynos.click();
		//WebElement showentrynos_100 = driver.findElement(By.xpath("//*[@id=\"example_length\"]/label/select/option[4]"));
		showentrynos.sendKeys("100");
		showentrynos.click();
		
		//Name Assert
		List<WebElement> lstwename = driver.findElements(byvarname); 

		for(int i=0 ; i < lstwename.size(); i++) {
			String foundmatchedname =  lstwename.get(i).getText();
			if(foundmatchedname.contains(name))
			{
				//System.out.println("Expected name is: " + name + " Actual name is:" + foundmatchedname);
				Assert.assertEquals(name, foundmatchedname);
				Actualname = foundmatchedname;
			}
		}
		//Position Assert
				List<WebElement> lstweposition = driver.findElements(byvarposition); 

				for(int i=0 ; i < lstweposition.size(); i++) {
					String foundmatchedposition =  lstweposition.get(i).getText();
					if(foundmatchedposition.contains(position))
					{
						//System.out.println("Expected Position is: " + position + " Actual Position is:" + foundmatchedposition);
						Assert.assertEquals(position, foundmatchedposition);
						Actualposition = foundmatchedposition;
					}
				}
				//Age Assert
				List<WebElement> lstweage = driver.findElements(byvarage); 

				for(int i=0 ; i < lstweage.size(); i++) {
					String foundmatchedage =  lstweage.get(i).getText();
					if(foundmatchedage.contains(age))
					{
						//System.out.println("Expected Age is: " + age + " Actual Age is:" + foundmatchedage);
						Assert.assertEquals(age, foundmatchedage);
						Actualage = foundmatchedage;
						
					}
				}
		
				returnValue = Actualname + " " + Actualposition+ " " + Actualage;
				
		return returnValue;
	}

	
	
}
