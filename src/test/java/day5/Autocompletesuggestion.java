package day5;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class Autocompletesuggestion {
	WebDriver driver = null;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome", false);
	}
	@Test
	public void f() {
		driver.get("https://jqueryui.com/autocomplete/");
		////*[@class='ui-autocomplete-input']
		////*[@id="tags"]
		By byvar = By.xpath("//*[@id=\"tags\"]");
		WebElement weselect = driver.findElement(byvar);
		driver.switchTo().frame(0);
		weselect.sendKeys("j");		
		List<WebElement> listOptions =  driver.findElements(By.xpath("//html/body/ul/li[*]"));
		int myindex = 0;
		for ( int i=0; i < listOptions.size() ; i++) {
			String strOption = listOptions.get(i).getText();
			System.out.println(i + " Option is : "+ strOption);
			if(strOption.equals("Javascript"))
			{
				
			}
		}
		driver.switchTo().parentFrame();
	}
	@AfterTest
	public void afterTest() {
		//driver.quit();
	}
}
