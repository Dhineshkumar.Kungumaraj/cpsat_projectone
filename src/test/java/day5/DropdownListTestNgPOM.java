package day5;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class DropdownListTestNgPOM {
	WebDriver driver;
	
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("ChrOME", false);
	}
	@Test
	public void f() {
		driver.get("file:///C:/Users/D826922/Desktop/Documents/Learning/CP-SAT%20files/CP-SAT%20Lab/WorkSpace/cpsat131mavenproject-master/src/test/resources/data/dropdown.html");
		WebElement dropdown = driver.findElement(By.xpath("/html/body/form/select"));
		Select Listdropdown = new Select(dropdown);
		List<WebElement> ListdropdownLT = Listdropdown.getOptions();
		for(int i=0;i<ListdropdownLT.size();i++)
		{
			String Listouput = ListdropdownLT.get(i).getText();
			System.out.println(Listouput);
		}
		
	}
	@AfterTest
	public void afterTest() {
		//driver.quit();
	}

}
