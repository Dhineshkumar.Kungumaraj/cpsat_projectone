package day5;

import org.testng.annotations.Test;

import org.testng.Assert;

import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class DatatableTestNg {

	WebDriver driver = null;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome", false);
	}

	@Test
	public void datatable() {
		driver.get("https://datatables.net/examples/styling/jqueryUI.html");

		WebElement showentrynos = driver.findElement(By.xpath("//*[@id=\"example_length\"]/label/select"));
		showentrynos.click();
		showentrynos.sendKeys("100");
		//WebElement showentrynos_100 = driver.findElement(By.xpath("//*[@id=\"example_length\"]/label/select/option[4]"));
		showentrynos.click();

		String strexpname = "Brenden Wagner";
		String strexpposition = "Software Engineer";
		String strexpage = "28";

		//*[@id="example"]/tbody/tr[*]/td[1] - name
		//*[@id="example"]/tbody/tr[*]/td[2] - position
		//*[@id="example"]/tbody/tr[*]/td[4] - age
		//*[@id=\"example\"]/tbody/tr[*]- all row

		By byvarname = By.xpath("//*[@id=\"example\"]/tbody/tr[*]/td[1]");
		By byvarposition = By.xpath("//*[@id=\"example\"]/tbody/tr[*]/td[2]");
		By byvarage = By.xpath("//*[@id=\"example\"]/tbody/tr[*]/td[4]");
		//By byvarallrow = By.xpath("//*[@id='example']/tbody/tr[*]");

		//Name Assert
		List<WebElement> lstwename = driver.findElements(byvarname); 

		for(int i=0 ; i < lstwename.size(); i++) {
			String foundmatchedname =  lstwename.get(i).getText();
			if(foundmatchedname.contains(strexpname))
			{
				System.out.println("Expected name is: " + strexpname + " Actual name is:" + foundmatchedname);
				Assert.assertEquals(strexpname, foundmatchedname);
				break;
			}
		}
		//Position Assert
				List<WebElement> lstweposition = driver.findElements(byvarposition); 

				for(int i=0 ; i < lstweposition.size(); i++) {
					String foundmatchedposition =  lstweposition.get(i).getText();
					if(foundmatchedposition.contains(strexpposition))
					{
						System.out.println("Expected Position is: " + strexpposition + " Actual Position is:" + foundmatchedposition);
						Assert.assertEquals(strexpposition, foundmatchedposition);
						break;
					}
				}
				//Age Assert
				List<WebElement> lstweage = driver.findElements(byvarage); 

				for(int i=0 ; i < lstweage.size(); i++) {
					String foundmatchedage =  lstweage.get(i).getText();
					if(foundmatchedage.contains(strexpage))
					{
						System.out.println("Expected Age is: " + strexpage + " Actual Age is:" + foundmatchedage);
						Assert.assertEquals(strexpage, foundmatchedage);
						break;
					}
				}
	}


	@AfterTest
	public void afterTest() {
		//driver.quit();
	}

}
