package day5;

import org.testng.annotations.Test;

import utils.DataReaders;
import utils.HelperFunctions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class DatatableTestNgPOMDP {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("Chrome", true);
	}
	
	@DataProvider
	public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException {
		Object[][] data=null;
		try {
			data = DataReaders.getExcelDataUsingPoi("src\\test\\resources\\data\\datatable.xlsx", "data");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
	
	@Test(dataProvider = "dp")
	public void f(String v1, String v2, String v3) {
		DatatableClass datatable = new DatatableClass(driver);
		driver.get("https://datatables.net/examples/styling/jqueryUI.html");
		String input1 = v1;
		String input2 = v2;
		String input3 = v3;
		System.out.println("\n");
		String expectedResult = input1+" "+ input2 +" "+ input3 ;
		String actualResult = datatable.Compare(input1, input2, input3);
		System.out.println("Actual Results is : "+actualResult+ "\n"+ "Expected Results is : "+expectedResult);
		System.out.println("\n");
		Assert.assertEquals(actualResult, expectedResult,"Results do not match");
		
	}

	@AfterTest
	public void afterTest() {
		//driver.quit();
	}

}
