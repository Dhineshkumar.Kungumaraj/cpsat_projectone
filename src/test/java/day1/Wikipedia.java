package day1;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Wikipedia {

	public static void main(String[] args) {

		WebDriver driver;
		//Chrome
		//System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		//driver = new ChromeDriver();// driver is ChromeDriver
		//FireFox
		System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
		driver = new FirefoxDriver();// driver is FirefoxDriver
		
		driver.manage().window().maximize();
		driver.get("https://www.wikipedia.org");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement englishlang = driver.findElement(By.xpath("//*[@class='central-featured-lang lang1']"));
		englishlang.click();
		WebElement searchwikipedia = driver.findElement(By.xpath("//*[@name='search']"));
		searchwikipedia.sendKeys("selenium");
		WebElement searchbtn = driver.findElement(By.xpath("//*[@name='go']"));
		searchbtn.click();
		String getTitle = driver.getTitle();
		if(getTitle.contains("Selenium - Wikipedia"))
		{
			System.out.println("Pass");
		}
		else
		{
			System.out.println("Fail");
		}
		driver.quit();
		System.out.println("Program executed");
	}

}
