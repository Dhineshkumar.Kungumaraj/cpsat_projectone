package day3;

import org.testng.annotations.Test;
import utils.HelperFunctions;
import org.testng.annotations.BeforeTest;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class ATASocialMediaHrefTestNg {

	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().window().maximize();
		driver.get("http://agiletestingalliance.org/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void f() {
		System.out.println("Test");
		System.out.println("");
		WebElement InitialScreenClose = driver.findElement(By.xpath("//*[@class='eicon-close']"));
		InitialScreenClose.click();
		List<WebElement> hrefsociallist = driver.findElements(By.xpath("//*[@id='custom_html-10']/div/ul/li[*]/b/b/a"));
		for(WebElement href : hrefsociallist)
		{
			System.out.println(href.getAttribute("href"));
		}
	}


@AfterTest
public void afterTest() {
	System.out.println("");
	System.out.println("After Test");
	driver.quit();
}

}
