package day3;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ATAEventsWindowSwitching {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().window().maximize();
		driver.get("https://www.ataevents.org/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void f() {
		System.out.println("Test");
		System.out.println("");
		WebElement InitialCloseicon = driver.findElement(By.xpath("//*[@class='eicon-close']"));
		InitialCloseicon.click();
		
		WebElement Link1 = driver.findElement(By.xpath("//a[contains(text(),'CP-WST')]"));
		WebElement Link2 = driver.findElement(By.xpath("//a[contains(text(),'Micro Workshop on Infrastructure')]"));
		WebElement Link3 = driver.findElement(By.xpath("//a[contains(text(),'CP-SAT(Python)')]"));
		WebElement Link4 = driver.findElement(By.xpath("//a[contains(text(),'CP-DOF')]"));
		WebElement Link5 = driver.findElement(By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[4]/div/div/div[1]/div/div/div/div/div/div/h3"));
		WebElement Link6 = driver.findElement(By.xpath("//a[contains(text(),'CP-SAT(C#)')]"));
		WebElement Link7 = driver.findElement(By.xpath("//a[contains(text(),'Global Testing Retreat 2020')]"));
		Link1.click(); 
		HelperFunctions.switchToWindow(driver, "CP-WST");
		String Link1Pagetitle = driver.getTitle();
		HelperFunctions.switchToWindow(driver, "Best DevOps, Selenium Testing, Web Services, Training");
		Link2.click();
		HelperFunctions.switchToWindow(driver, "Micro Workshop on Infrastructure");
		String Link2Pagetitle = driver.getTitle();
		HelperFunctions.switchToWindow(driver, "Best DevOps, Selenium Testing, Web Services, Training");
		Link3.click();
		HelperFunctions.switchToWindow(driver, "CP-SAT(Python)");
		String Link3Pagetitle = driver.getTitle();
		HelperFunctions.switchToWindow(driver, "Best DevOps, Selenium Testing, Web Services, Training");
		Link4.click();
		HelperFunctions.switchToWindow(driver, "CP-DOF");
		String Link4Pagetitle = driver.getTitle();
		HelperFunctions.switchToWindow(driver, "Best DevOps, Selenium Testing, Web Services, Training");
		//WebDriverWait wait = new WebDriverWait(driver, 25); 
		//wait.until(ExpectedConditions.elementToBeClickable(Link5));
		Link5.click(); 
		HelperFunctions.switchToWindow(driver, "Agile Testing Alliance - Virtual Meetup 4");
		String Link5Pagetitle = driver.getTitle();
		Assert.assertEquals(Link5Pagetitle, "Agile Testing Alliance - Virtual Meetup 4");
		HelperFunctions.switchToWindow(driver, "Best DevOps, Selenium Testing, Web Services, Training");
		Link6.click(); 
		HelperFunctions.switchToWindow(driver, "CP-SAT- C Sharp");
		String Link6Pagetitle = driver.getTitle();
		HelperFunctions.switchToWindow(driver, "Best DevOps, Selenium Testing, Web Services, Training");
		Link7.click();
		HelperFunctions.switchToWindow(driver, "Global Testing Retreat 2020");
		String Link7Pagetitle = driver.getTitle();
		HelperFunctions.switchToWindow(driver, "Best DevOps, Selenium Testing, Web Services, Training");
		System.out.println("");
		System.out.println(Link1Pagetitle+"\n"+Link2Pagetitle+"\n"+Link3Pagetitle+"\n"+Link4Pagetitle+"\n"+
		Link5Pagetitle+"\n"+Link6Pagetitle+"\n"+Link7Pagetitle);
		
		
	}
	@AfterTest
	public void afterTest() {
		System.out.println("");
		System.out.println("After Test");
		driver.quit();
	}

}
