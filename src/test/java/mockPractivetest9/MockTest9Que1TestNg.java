package mockPractivetest9;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest9Que1TestNg {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ndtv.com/business"); // (1/2 Mark)	
	}

	@Test
	public void f() throws InterruptedException {

		String Exptooltipmoney= "Money";
		System.out.println("Top stories Href link:"); 
		List<WebElement> href_topstories = driver.findElements(By.xpath("/html/body/div[5]/div/div/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div/ul/li[*]/p/a"));
		for(WebElement ele: href_topstories)
		{
			System.out.println(ele.getAttribute("href")); //(1 mark) 
		}
		System.out.println("");

		WebElement tooltip_money = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[2]/div[1]/ul/li[6]/a"));
		String Acttooltipmoney = tooltip_money.getAttribute("title");
		Assert.assertEquals(Acttooltipmoney, Exptooltipmoney);//(2 marks)

		Actions action = new Actions(driver);
		action.moveToElement(tooltip_money);
	    action.keyDown(Keys.CONTROL);
	    action.click();
	    action.keyDown(Keys.CONTROL).build().perform();

		Thread.sleep(3000);
		HelperFunctions.switchToWindow(driver, "News on Personal Finance");
		Thread.sleep(3000);
		WebElement alert = driver.findElement(By.xpath("//*[@id=\"__cricketsubscribe\"]/div[2]/div[2]/a[1]"));
		alert.click(); //Click No Thanks
		System.out.println("Title of Money page is:"); 
		System.out.println(driver.getTitle());//(2 marks)
		System.out.println(""); 
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/money.png");//(1 mark)
		System.out.println("Top Stories of Money page is:");
		List<WebElement> href_moneytopstories = driver.findElements(By.xpath("//*[@id=\"ins_storylist\"]/ul/li[*]/div[2]/h2/a"));
		System.out.println(href_moneytopstories.size());

		for(WebElement ele: href_moneytopstories)
		{ 
				System.out.println(ele.getAttribute("href")); //(1 mark)
		}	

		HelperFunctions.switchToWindow(driver, "Business News, Market Updates");
		System.out.println("");
		System.out.println("Title of Business page is");
		System.out.println(driver.getTitle());// (2.5 marks)

	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
