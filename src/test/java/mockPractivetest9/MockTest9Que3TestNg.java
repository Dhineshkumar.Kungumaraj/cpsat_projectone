package mockPractivetest9;

import org.testng.annotations.Test;

import utils.DataWriter;

import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest9Que3TestNg {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ndtv.com/business/your-money");// (1/2 Mark)
	}
	@Test
	public void f() throws InterruptedException {
		
		String outputFileName = "src/test/resources/data/Result_ndtv.xlsx";
		String outputSheet = "Sheet1";
		boolean flag = true;
		
		WebElement alert = driver.findElement(By.xpath("//*[@id=\"__cricketsubscribe\"]/div[2]/div[2]/a[1]"));
		WebDriverWait wait = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(9000));  
		wait.until(ExpectedConditions.elementToBeClickable(alert));
		if(alert.isDisplayed())
		{
		alert.click(); //Click No Thanks
		}
		//value //*[@id="header-data"]/a[7]/span/span[1]
		//value change  //*[@id="header-data"]/a[7]/span/span[2]/span
		//% change //*[@id="header-data"]/a[7]/span/span[2]
		driver.switchTo().frame(1);
		WebElement Silvervalue = driver.findElement(By.xpath("//*[@id=\"header-data\"]/a[7]/span/span[1]"));
		WebDriverWait wait1 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(15000));  
		wait1.until(ExpectedConditions.elementToBeClickable(Silvervalue));
		String value = Silvervalue.getText();
		System.out.println(value);	
		
		WebElement Silvervaluechange = driver.findElement(By.xpath("//*[@id=\"header-data\"]/a[7]/span/span[2]/span"));
		WebDriverWait wait2 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(9000));  
		wait2.until(ExpectedConditions.elementToBeClickable(Silvervaluechange));
		String valuechange = Silvervaluechange.getText();
		System.out.println(valuechange);
		
		WebElement Silverpercentagechange = driver.findElement(By.xpath("//*[@id=\"header-data\"]/a[7]/span/span[2]"));
		String Silverpercentage =Silverpercentagechange.getText().substring(9, 15);
		System.out.println(Silverpercentage);
		//System.out.println(Silverpercentagechange.getText());

		String Background = driver.findElement(By.xpath("//*[@id='header-data']/a[7]/span/span[2]")).getCssValue("background-color"); 
		System.out.println("Background color is : "+Background);

		String BG_RGB = Background.replace("rgba(", "rgb("); 
		String RGB =BG_RGB.substring(0, 14) + ")" ; 
		String RGB_Value = RGB.substring(3);
		System.out.println("Background color in RGB is : "+ RGB ); 
		System.out.println("RGB value is : "+ RGB_Value); 
		Assert.assertEquals("(12, 162, 2)", RGB_Value);
		
		ArrayList<String> list=new ArrayList<String>();//Creating arraylist    
		list.add(0, value);  
		list.add(1, valuechange);    
		list.add(2, Silverpercentage);    
		list.add(3, RGB_Value);    

		try {
			DataWriter.writeToFile(outputFileName, outputSheet,list,flag);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement nifty = driver.findElement(By.xpath("//*[@id=\"header-data\"]/a[5]/span/span[2]"));
		WebDriverWait wait3 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(9000));  
		wait3.until(ExpectedConditions.elementToBeClickable(nifty));
		nifty.click();
		Thread.sleep(3000);

		WebElement alphabetical= driver.findElement(By.xpath("//*[@id='Alphabetical']"));
		WebDriverWait wait4 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(9000));  
		wait4.until(ExpectedConditions.elementToBeClickable(alphabetical));
		//JavascriptExecutor executor = (JavascriptExecutor)driver;
		//executor.executeScript("arguments[0].click();", alphabetical);
		
		Actions actions = new Actions(driver);
		actions.moveToElement(alphabetical).click().perform();
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("arguments[0].scrollIntoView();", alphabetical);
		Thread.sleep(5000);
		System.out.println("successfull");
		
		utils.HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/alphabetical.png");
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
