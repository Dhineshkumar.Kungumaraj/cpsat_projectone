package mockPracticetest10;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest10Que2TestNG {

	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ndtv.com/business/your-money");// (1/2 Mark)
	}
	@Test
	public void f() throws InterruptedException {
		//			a. Open the website: https://www.ndtv.com/business/your-money (1/2 mark)
		//			b. Click on SITE and take screenshot at this instance (2 marks) (Screenshot is
		//			needed at this stage)
		//			c. Select “FNO” from dropdown and take screenshot (1 mark)
		//			CP-SAT Practical Exam: Set 4 – October 2019
		//			© CP-SAT - Agile Testing Alliance: Version 1 Set 4, October 2019 3
		//			d. Search for “TCS” (1/2 mark)
		//			e. On the search result page scroll down to look for Video Results. Fetch out the
		//			total number of video results for TCS, this number should be printed as an
		//			integer on the Eclipse console. (2 marks)
		//			f. Assert that the video results number fetched is greater than 1 (one) (2
		//			marks)

		Thread.sleep(3000);
		WebElement alert = driver.findElement(By.xpath("//*[@id=\"__cricketsubscribe\"]/div[2]/div[2]/a[1]"));
		alert.click(); //Click No Thanks

		driver.switchTo().frame(1);

		WebElement dropdown = driver.findElement(By.xpath("//*[@id=\"header\"]/div/div[2]/div"));
		dropdown.click();
		Thread.sleep(8000);
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/SiteDropdown.png");//(2 mark)

		WebElement fnoselect = driver.findElement(By.xpath("/html/body/div/header/header/div/div[2]/div/select"));
		//driver.switchTo().parentFrame();
		Select selectObject = new Select(fnoselect);
		selectObject.selectByIndex(4);
		//Fnodropdown.sendKeys("FNO");
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/FNODropdown.png");//(1 mark)
		WebElement Searchbox = driver.findElement(By.xpath("//*[@id=\"search\"]"));
		Searchbox.click();
		Searchbox.sendKeys("TCS");
		Searchbox.sendKeys(Keys.ENTER);//(1/2 marks)
		Thread.sleep(3000);
		WebElement alert2 = driver.findElement(By.xpath("//*[@id=\"__cricketsubscribe\"]/div[2]/div[2]/a[1]"));
		alert2.click(); //Click No Thanks

		List<WebElement> videoresults = driver.findElements(By.xpath("//*[@id=\"Related\"]/ul[*]/li[1]/p[2]/a"));
		int videocount = videoresults.size();
		System.out.println("No.of Video's is: "+videocount);
		assert videoresults.size()>1 : "Less than or equal to one"; //(2 marks)

		//		g. Get Href of the first video result (1 mark)
		WebElement Videoresultsone = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[1]/div[2]/div[1]/div[5]/div[1]/div[1]/div[3]/ul/ul[1]/li[1]/a"));
		Videoresultsone.getAttribute("href");//(1 mark)
		//		h. Navigate to the video URL (1 mark)		
		Videoresultsone.click(); //(1 mark)

	}
	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
