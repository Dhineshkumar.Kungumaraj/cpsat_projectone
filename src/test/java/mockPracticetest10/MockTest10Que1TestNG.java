package mockPracticetest10;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest10Que1TestNG {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ndtv.com/business"); // (1/2 Mark)
	}
	@Test
	public void f() throws InterruptedException {

		String Exptooltipcorporate= "Corporates";

		List<WebElement> href_topstories = driver.findElements(By.xpath("/html/body/div[5]/div/div/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div/ul/li[*]/p/a"));
		for(WebElement ele: href_topstories)
		{
			System.out.println(ele.getAttribute("href")); //(1 mark)
		}

		WebElement tooltip_corporates = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[2]/div[1]/ul/li[7]/a"));
		String Acttooltipcorporate = tooltip_corporates.getAttribute("title");
		Assert.assertEquals(Acttooltipcorporate, Exptooltipcorporate);//(2 marks)

		Actions action = new Actions(driver);
		action.moveToElement(tooltip_corporates);
	    action.keyDown(Keys.CONTROL);
	    action.click();
	    action.keyDown(Keys.CONTROL).build().perform();

		Thread.sleep(5000);
		HelperFunctions.switchToWindow(driver, "Corporate News, Company News");
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/Corporates.png");//(1 mark)
		int i=0;
		for(i=1; i<=3;i++) {
		By byvar = By.xpath("//*[@id=\"ins_storylist\"]/ul/li["+i+"]/div[2]/h2/a");
		List<WebElement> href_corporates = driver.findElements(byvar);
		for(WebElement ele1: href_corporates)
		{
				System.out.println(ele1.getAttribute("href")); //(1 mark)

		}
		}
		System.out.println(driver.getTitle()); // (2.5 marks)		
		HelperFunctions.switchToWindow(driver, "Business News, Market Updates");
		System.out.println(driver.getTitle());

	}
	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
