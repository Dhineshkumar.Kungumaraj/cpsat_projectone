package mockPracticetest10;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest10Que4TestNG {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ndtv.com/business/");//(Marks)
	}

	@Test
	public void TestNgPOM() throws InterruptedException {
		
		WebElement hreftopbusiness = driver.findElement(By.xpath("//a[text()='Business']"));
		String Exphreftopbusiness = hreftopbusiness.getAttribute("href");
		String Expmenutopbusiness = hreftopbusiness.getText();
		System.out.println(Expmenutopbusiness);
		
		System.out.println("Top Href's:");
		List<WebElement> tophref = driver.findElements(By.xpath("//*[@class='more']/a"));
		for(WebElement ele:tophref)
		{
			System.out.println(ele.getAttribute("href"));//(Marks)
		}
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)"); //This will scroll the web page till end.
		
		List<WebElement> bottomhref = driver.findElements(By.xpath("/html/body/div[5]/div/div/div[3]/div/div/div/div/div[2]/div[4]/ul/li/a"));
		System.out.println("Bottom Href's:");
		for(WebElement ele:bottomhref)
		{
			
			System.out.println((String)ele.getAttribute("href"));//(Marks)
		}
		WebElement hrefbottombusiness = driver.findElement(By.xpath("/html/body/div[5]/div/div/div[3]/div/div/div/div/div[2]/div[4]/ul/li[2]/a"));
		String Acthrefbotttonbusiness = hrefbottombusiness.getAttribute("href");
		
		Assert.assertEquals(Acthrefbotttonbusiness, Exphreftopbusiness);//(Marks)
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
