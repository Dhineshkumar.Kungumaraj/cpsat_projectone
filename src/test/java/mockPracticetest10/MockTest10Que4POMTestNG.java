package mockPracticetest10;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest10Que4POMTestNG {

	WebDriver driver = null;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ndtv.com/business/");

	}

	@Test
	public void TestNgPOM() {

		MockTest10Que4Class href = new MockTest10Que4Class(driver);
		
		String ActualtopBusinesslink = href.hreftopbusinessActual();
		String ExpectedbottomBusinesslink = href.hrefbottombusinessexpected();

		//1
		href.hreftoplist();
		//2
		href.hrefbottomlist();
		//3
		Assert.assertEquals(ActualtopBusinesslink, ExpectedbottomBusinesslink);

	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
