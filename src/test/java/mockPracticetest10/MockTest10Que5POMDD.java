package mockPracticetest10;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest10Que5POMDD {

	WebDriver driver;
	
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ndtv.com/business/");
	}

	@Test(dataProvider = "dp")
	public void f(String v1, String v2) {
		
		String input1 = v1;
		String input2 = v2;
		
		MockTest10Que4Class href = new MockTest10Que4Class(driver);
		
		String Actmenu = href.MenutopbusinessActualDD(input1);
		String Acthref = href.hreftopbusinessActualDD(input1);
		
		Assert.assertEquals(Actmenu, input1);
		Assert.assertEquals(Acthref, input2);
	}

	@DataProvider
	public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		Object[][] data=null;
		data = utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\ndtv.com.xlsx", "data");
		return data;
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
