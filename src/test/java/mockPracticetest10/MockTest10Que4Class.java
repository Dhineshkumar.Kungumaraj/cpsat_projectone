package mockPracticetest10;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class MockTest10Que4Class {

	WebDriver driver;

	public MockTest10Que4Class(WebDriver driver) {
		this.driver = driver;
	}

	By byvar1 = By.xpath("//a[text()='Business']"); //single ele top business
	By byvar2 = By.xpath("/html/body/div[5]/div/div/div[3]/div/div/div/div/div[2]/div[4]/ul/li[2]/a"); //single ele bottom business
	By byvar3 = By.xpath("//*[@id='RHS_WYSIWYG']/div/div/a"); //list top
	By byvar4 = By.xpath("//*[@class='more']/a"); //list bottom

	// Print all Top Href link 
	public void hreftoplist()
	{		
		List<WebElement> tophref = driver.findElements(byvar3);
		System.out.println("Top Href's links are:");
		for(WebElement ele:tophref)
		{
			System.out.println(ele.getAttribute("href"));//(Marks)
		}
		System.out.println("");
	}
	//Print All Bottom Href link
	public void hrefbottomlist()
	{		
		List<WebElement> bottomhref = driver.findElements(byvar4);
		System.out.println("Bottom Href's Links are:");
		for(WebElement ele:bottomhref)
		{
			System.out.println(ele.getAttribute("href"));//(Marks)
		}
		System.out.println("");
	}

	//Business Href Actual Value TOP
	String hreftopbusinessActual()
	{
		String returnvalue = null;
		WebElement hreftopbusiness = driver.findElement(byvar1);
		returnvalue = hreftopbusiness.getAttribute("href");
		return returnvalue;
	}

	//Business Href Actual Value BOTTOM
	String hrefbottombusinessexpected()
	{
		String returnvalue = null;
		WebElement hrefbottombusiness = driver.findElement(byvar2);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)"); //This will scroll the web page till end.
		returnvalue = hrefbottombusiness.getAttribute("href");
		return returnvalue;
	}

	String MenutopbusinessActualDD(String input1)
	{		
		String returnvalue = null;
		List<WebElement> tophref = driver.findElements(byvar3);
		boolean matchFound = false; 
		for( WebElement ele : tophref) {
			String acttopvalue = ele.getText();
			if(acttopvalue.equals(input1)) {
				returnvalue = (String)ele.getText();
				matchFound = true;
				break;
			}	
		}
		return returnvalue;
		
	}

	String hreftopbusinessActualDD(String input1)
	{		
		String returnvalue = null;
		List<WebElement> tophref = driver.findElements(byvar3);
		boolean matchFound = false; 
		for( WebElement ele : tophref) {
			String acttopvalue = ele.getText();
			if(acttopvalue.contains(input1)) {

				returnvalue = ele.getAttribute("href");
				matchFound = true;
				break;
			}	
		}
		return returnvalue;
		
	}
}
