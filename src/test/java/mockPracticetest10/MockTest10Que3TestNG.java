package mockPracticetest10;

import org.testng.annotations.Test;
import utils.HelperFunctions;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest10Que3TestNG {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ndtv.com/business/your-money");
	}

	@Test
	public void f() throws InterruptedException {
//		
//			a. Open the website: https://www.ndtv.com/business/your-money
//			b. For Gold
//			Please get the current value on the website (e.g. 38352.00) and the change in
//			number e.g. 462.00 and the percentage change 1.22 % - these values are for
//			representation purpose as the value will vary based on the day the test is
//			being run. (3 marks)
//			c. Check whether the background color of above is Red Or Green, for Red assert
//			the RGB value for (182, 0,0) for Green assert the RGB value for (12, 162, 2) (3
//			marks).
//			d. Click on NIFTY50 (1 mark)
//			This will open the nifty50 page
//			e. Scroll down and click on “By Gain” (1 mark)
//			This will arrange the scripts in order of highest to lowest gain
//			Hover on the first name displayed and take screenshot at this instance
//			(3 marks)
//			g. For the first name displayed, print Name of the script (company), Cell’s RGB
//			value and % (as integer) on console (2 marks)
//			h. Assert that the company names are alphabetically sorted row wise (2 marks)

		
		WebElement alert = driver.findElement(By.xpath("//*[@id=\"__cricketsubscribe\"]/div[2]/div[2]/a[1]"));
		WebDriverWait wait = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(9000));  
		wait.until(ExpectedConditions.elementToBeClickable(alert));
		if(alert.isDisplayed())
		{
		alert.click(); //Click No Thanks
		}
		driver.switchTo().frame(1);		
		WebElement goldpricetoday = driver.findElement(By.xpath("//*[@id=\"header-data\"]/a[6]/span/span[1]"));
		WebDriverWait wait1 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(15000));  
		wait1.until(ExpectedConditions.elementToBeClickable(goldpricetoday));
		System.out.println(goldpricetoday.getText()); 
		
		WebElement goldpricechange = driver.findElement(By.xpath("//*[@id=\"header-data\"]/a[6]/span/span[2]/span"));
		WebDriverWait wait2 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(15000));  
		wait2.until(ExpectedConditions.elementToBeClickable(goldpricechange));
		System.out.println(goldpricechange.getText());
		System.out.println(goldpricechange.getCssValue("background-color"));
		//goldpricechange.getCssValue("color");//

		WebElement goldpricechangepercentage = driver.findElement(By.xpath("//*[@id=\"header-data\"]/a[6]/span/span[2]"));
		WebDriverWait wait3 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(15000));  
		wait3.until(ExpectedConditions.elementToBeClickable(goldpricechangepercentage));
		System.out.println(goldpricechangepercentage.getText().substring(9, 15)); 
		
		String Background = driver.findElement(By.xpath("//*[@id='header-data']/a[6]/span/span[2]/span")).getCssValue("background-color");
		System.out.println("Background color is : "+Background);

		String BG_RGB = Background.replace("rgba(", "rgb("); 
		String RGB =BG_RGB.substring(0, 14) + ")" ; 
		String RGB_Value = RGB.substring(3);
		System.out.println("Background color in RGB is : "+ RGB ); 
		System.out.println("RGB value is : "+ RGB_Value);
		String ExpRGB = "(12, 162, 2)";
		Assert.assertEquals("RGB_Value", ExpRGB);
		
		
		WebElement nifty = driver.findElement(By.xpath("//*[@id=\"header-data\"]/a[4]/span/span[2]"));
		WebDriverWait wait4 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(9000));  
		wait4.until(ExpectedConditions.elementToBeClickable(nifty));
		nifty.click();
		Thread.sleep(3000);
		
		WebElement byalphabet = driver.findElement(By.xpath("//*[@id='Alphabetical']"));
		WebDriverWait wait5 = new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(9000));  
		wait5.until(ExpectedConditions.elementToBeClickable(byalphabet));
		
		Actions actions = new Actions(driver);
		actions.moveToElement(byalphabet).click().perform();
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		js1.executeScript("arguments[0].scrollIntoView();", byalphabet);
		Thread.sleep(5000);
		
		WebElement byalphabet_firstelement = driver.findElement(By.xpath("//*[@id=\"peerlist1\"]/li[1]/a"));
		System.out.println(byalphabet_firstelement.getText());
		
		String Background1 = driver.findElement(By.cssSelector("//*[@id=\\\"peerlist1\\\"]/li[1]/a")).getCssValue("background-color");
		System.out.println("Background color is : "+Background1);

		String BG_RGB1 = Background1.replace("rgba(", "rgb("); 
		String RGB1 =BG_RGB1.substring(0, 14) + ")" ; 
		String RGB_Value1 = RGB1.substring(3);
		System.out.println("Background color in RGB is : "+ RGB1 ); 
		System.out.println("RGB value is : "+ RGB_Value1); 
		String ExpRGB1 = "(182, 0, 0)";
		Assert.assertEquals(RGB_Value1,ExpRGB1 );
		
		WebElement bygain_firstelementpercent = driver.findElement(By.xpath("//*[@id=\"peerlist2\"]/li[1]/a/text()[2]"));
		String percentage = bygain_firstelementpercent.getText().substring(0, 9);	
		//Integer Percentage = Integer.parseInt(percentage);
		System.out.println(percentage);
		//HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/Firstelementbygain.png");//(1 mark)

		
		
		//*[@id="header-data"]/a[6]/h5   
	}

	@AfterTest
	public void afterTest() {
		//driver.quit();
	}

}
