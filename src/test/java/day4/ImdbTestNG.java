package day4;

import org.testng.annotations.Test;
import utils.HelperFunctions;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class ImdbTestNG {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");
		driver = HelperFunctions.createAppropriateDriver("chrome");
	}
	@Test
	public void f() {
		String expDirector = "Mastan";
		String expstar = "Shah Rukh Khan";
		driver.get("https://www.imdb.com/");
		WebElement Mainsearch = driver.findElement(By.xpath("//*[@type='text']"));
		Mainsearch.sendKeys("Baazigar");
		Mainsearch.sendKeys(Keys.ENTER);
		List<WebElement> Directors = driver.findElements(By.xpath("//*[@class='inline' and contains(text(),'Director')]//following-sibling::*"));
		for(WebElement Directorsearch : Directors)
		{
			if(Directorsearch.getText().contains("Mastan"))
			{
				System.out.println(Directorsearch.getText());
				break;
			}
		}
		List<WebElement> stars = driver.findElements(By.xpath("//*[@class='inline' and contains(text(),'star')]//following-sibling::*"));
		for(WebElement starsearch : stars)
		{
			if(starsearch.getText().contains("Mastan"))
			{
				System.out.println(starsearch.getText());
				break;
			}
		}		
	}
	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
