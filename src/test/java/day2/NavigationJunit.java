package day2;

import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import utils.HelperFunctions;

public class NavigationJunit {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.out.println("Before");
		driver = HelperFunctions.createAppropriateDriver("Chrome");
		driver.manage().window().maximize();
		driver.navigate().to("https://www.google.com/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@Test
	public void test() {
		System.out.println("Test");
		driver.navigate().back();
		System.out.println(driver.getTitle()); 
		driver.navigate().forward();
		System.out.println(driver.getTitle()); 
		driver.navigate().forward();
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("After");
		driver.quit();
	}



}
