package day2;

import org.testng.annotations.Test;
import utils.HelperFunctions;
import org.testng.annotations.BeforeTest;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class WikipediaTestNg {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		System.out.println("BeforeTest");
		/*Chrome
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();// driver is ChromeDriver
		//FireFox
		//System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
		//driver = new FirefoxDriver();// driver is FirefoxDriver */
		driver = HelperFunctions.createAppropriateDriver("Chrome");
		driver.manage().window().maximize();
		driver.get("https://www.wikipedia.org");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@Test 
	public void f() {
		System.out.println("Test");
		WebElement englishlang = driver.findElement(By.xpath("//*[@class='central-featured-lang lang1']"));
		englishlang.click();
		WebElement searchwikipedia = driver.findElement(By.xpath("//*[@name='search']"));
		searchwikipedia.sendKeys("selenium");
		WebElement searchbtn = driver.findElement(By.xpath("//*[@name='go']"));
		searchbtn.click();
		String getTitle = driver.getTitle();
		if(getTitle.contains("Selenium - Wikipedia"))
		{
			System.out.println("Pass");
		}
		else
		{
			System.out.println("Fail");
		}
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/Selenium-Wikipedia.png"); 
		Assert.assertEquals(getTitle, "Selenium - Wikipedia");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("AfterTest");
		driver.quit();
	}

}
