package day2;

import org.testng.annotations.Test;
import utils.HelperFunctions;
import org.testng.annotations.BeforeTest;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class AlertHandlingTestNg {

	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().window().maximize();
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test
	public void JSAlert(){
		System.out.println("Testing Javascript Alert");
		WebElement JsAlertbtn = driver.findElement(By.xpath("//*[contains(text(),'Click for JS Alert')]"));
		JsAlertbtn.click();
		Alert alert = driver.switchTo().alert();
		alert.accept();	
		String result = driver.findElement(By.xpath("//p[@id='result']")).getText();
		Assert.assertEquals(result, "You successfuly clicked an alert");
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/JSAlertOK.png");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void JSConfirm(){
		System.out.println("Testing JavaScript Confirm - Ok and Cancel");
		WebElement JSConfirmbtn = driver.findElement(By.xpath("//*[contains(text(),'Click for JS Confirm')]"));
		JSConfirmbtn.click();
		//Clicking OK
		Alert alert = driver.switchTo().alert();
		alert.accept();	
		String result = driver.findElement(By.xpath("//p[@id='result']")).getText();
		Assert.assertEquals(result, "You clicked: Ok");
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/JSConfirmOK.png");
		//Click Cancel
		JSConfirmbtn.click();
		Alert alert1 = driver.switchTo().alert();
		alert1.dismiss();
		String result1 = driver.findElement(By.xpath("//p[@id='result']")).getText();
		Assert.assertEquals(result1, "You clicked: Cancel");
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/JSConfirmCancel.png");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void JSPrompt(){
		System.out.println("Testing JavaScript Prompt - Enter Value > Ok and Enter Value > Cancel");
		WebElement JsPromptbtn = driver.findElement(By.xpath("//*[contains(text(),'Click for JS Prompt')]"));
		JsPromptbtn.click();
		//Clicking OK
		Alert alert = driver.switchTo().alert();
		alert.sendKeys("Selenium");	
		alert.accept();
		String result = driver.findElement(By.xpath("//p[@id='result']")).getText();
		Assert.assertEquals(result, "You entered: Selenium");
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/JSPromptSeleniumOK.png");
		//Click Cancel
		JsPromptbtn.click();
		Alert alert1 = driver.switchTo().alert();
		alert1.sendKeys("Selenium");	
		alert1.dismiss();
		String result1 = driver.findElement(By.xpath("//p[@id='result']")).getText();
		Assert.assertEquals(result1, "You entered: null");
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/JSPromptSeleniumCancel.png");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterTest
	public void afterTest() {
		System.out.println("After Test");
		driver.quit();
	}
}
