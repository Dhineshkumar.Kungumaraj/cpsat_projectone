package day2;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.lang.model.util.Elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class AmazonSearchTestNg {

	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");
		System.out.println("");
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().window().maximize();
		driver.get("https://www.amazon.in/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	@Test
	public void Descriptionprice() {
		System.out.println("Test");
		System.out.println("");
		WebElement Search = driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']"));
		Search.sendKeys("roasted coffee beans");
		driver.findElement(By.xpath("//input[@type='submit' and @class='nav-input']")).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<WebElement> searched_item_desc = driver.findElements(By.xpath("//span[contains(text(),'Roasted Coffee Beans')]"));
		System.out.println("Item Description is,");
		System.out.println("");
		for(WebElement description: searched_item_desc)
		{
			System.out.println(description.getText());
		}
		System.out.println("");
		System.out.println("Their Respective Prices are,");
		System.out.println("");
		List<WebElement> Itemprice = driver.findElements(By.xpath("//span[contains(text(),'Roasted Coffee Beans')]//ancestor::div[@class='a-section a-spacing-medium']//span[@class='a-price-whole']"));
		for(WebElement price: Itemprice)
		{
			String Description_ItemPrice ="Rs."+price.getText();
			System.out.println(Description_ItemPrice);
		}
	}
	@AfterTest
	public void afterTest() {
		System.out.println("");
		System.out.println("After Test");
		driver.quit();
	}

}
