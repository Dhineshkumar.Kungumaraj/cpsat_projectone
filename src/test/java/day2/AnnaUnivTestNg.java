package day2;

import org.testng.annotations.Test;
import utils.HelperFunctions;
import org.testng.annotations.BeforeTest;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class AnnaUnivTestNg {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");
		driver = HelperFunctions.createAppropriateDriver("Chrome");
		driver.manage().window().maximize();
		driver.get("https://annauniv.edu/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@Test
	public void f() throws InterruptedException {
		System.out.println("Test");
		WebElement department = driver.findElement(By.xpath("//a[contains(text(),'Departments')]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", department);
		
		WebElement Foce = driver.findElement(By.xpath("//*[@name='link13']"));
		//Action class
		Actions action = new Actions(driver);
		WebElement Ocean = driver.findElement(By.xpath("//*[@id='menuItemText32']"));
		action.moveToElement(Foce).moveToElement(Ocean).click().perform();
		System.out.println(driver.getTitle()); 
		Thread.sleep(4000);

	}

	@AfterTest
	public void afterTest() {
		System.out.println("After Test");
		driver.quit();
	}

}
