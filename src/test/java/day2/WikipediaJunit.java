package day2;

import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WikipediaJunit {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		//Chrome
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();// driver is ChromeDriver
		//FireFox
		//System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
		//driver = new FirefoxDriver();// driver is FirefoxDriver
		driver.manage().window().maximize();
		driver.get("https://www.wikipedia.org");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@Test
	public void test() {
		WebElement englishlang = driver.findElement(By.xpath("//*[@class='central-featured-lang lang1']"));
		englishlang.click();
		WebElement searchwikipedia = driver.findElement(By.xpath("//*[@name='search']"));
		searchwikipedia.sendKeys("selenium");
		WebElement searchbtn = driver.findElement(By.xpath("//*[@name='go']"));
		searchbtn.click();
		String getTitle = driver.getTitle();
		if(getTitle.contains("Selenium - Wikipedia"))
		{
			System.out.println("Pass");
		}
		else
		{
			System.out.println("Fail");
		}
	}
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
