package day2;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class AtaEventsOrgTestNg {

	WebDriver driver;
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test");
		driver = HelperFunctions.createAppropriateDriver("Chrome");
		driver.manage().window().maximize();
		driver.get("https://www.ataevents.org/");
		String getTitle = driver.getTitle();
		System.out.println(getTitle);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement Initialpopup = driver.findElement(By.xpath("//*[@class='eicon-close']"));
		Initialpopup.click();
	}
	
	@Test
	public void f() {
		System.out.println("Test");
		WebElement CPDOF = driver.findElement(By.xpath("//a[text()='CP-DOF Instructor Led Online Program']"));
		CPDOF.click();
		HelperFunctions.switchToWindow(driver, "CP-DOF");
		String getTitle1 = driver.getTitle();
		System.out.println(getTitle1);
		
	}

	@AfterTest
	public void afterTest() {
		System.out.println("AfterTest");
		driver.quit();
	}

}
