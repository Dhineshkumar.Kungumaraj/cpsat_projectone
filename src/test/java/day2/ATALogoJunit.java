package day2;

import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.HelperFunctions;

public class ATALogoJunit {

	WebDriver driver;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("Before");
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().window().maximize();
		driver.get("https://agiletestingalliance.org/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}
	
	@Test
	public void test() {
		System.out.println("Test");
		WebElement InitialCloseicon = driver.findElement(By.xpath("//*[@class='eicon-close']"));
		String text = driver.findElement(By.xpath("//*[@class='eicon-close']")).getText();
		System.out.println(text);
		InitialCloseicon.click();
		WebElement ATALogo = driver.findElement(By.xpath("//*[@class='no-sticky-logo']"));
		//Get width of element.
        int ImageWidth = ATALogo.getSize().getWidth();
        //Get height of element.
        int ImageHeight = ATALogo.getSize().getHeight();  
        System.out.println("ATALogo width is "+ImageWidth+" pixels & height is "+ImageHeight+" pixels");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("After");
		driver.quit();
	}



}
