package javaproject;

public class Polmorphism {

	public void addition(int a,int b) {
		int c= a+b;
		System.out.println(c);
	}
	public void addition(double a,double b) {
		double c= a+b;
		System.out.println(c);
	}
	public void addition(String name1, String name2) {
		String c= name1+name2;
		System.out.println(c);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Overridden methods
		Polmorphism addobj = new Polmorphism();
		addobj.addition(3, 4);
		addobj.addition(5.1, 3.1);
		addobj.addition("Good", " Day");
	}

}
