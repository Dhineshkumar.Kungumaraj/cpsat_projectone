package javaproject;

class inheritancesample{

	public int addition(int i)
	{
		return i+i;
	}
}

public class Inheritance extends inheritancesample {

	public static void main(String[] args) {
		Inheritance object = new Inheritance();
		int sum = object.addition(2);
		System.out.println(sum);
	}
}
