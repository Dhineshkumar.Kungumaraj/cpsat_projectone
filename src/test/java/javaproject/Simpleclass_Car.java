package javaproject;

public class Simpleclass_Car {


	public String Cartype(String carname)
	{
		return carname;
	}
	public int accelaratorincrease(int speed)
	{
		speed = speed+5;
		return speed;
	}
	public int applybreak()
	{	int speed = -5;
		return speed;
	}
	public static void main(String[] args) {
		// Cartype method
		Simpleclass_Car car = new Simpleclass_Car();
		String Cartype = car.Cartype("Renault Duster");
		System.out.println(Cartype);
		String Cartype1 = car.Cartype("Renault Kwid");
		System.out.println(Cartype1);
		String Cartype2 = car.Cartype("Renault Climber");
		System.out.println(Cartype2);

		//accelaratorincrease method
		int currentspeed = 0;
		int speedincrease = car.accelaratorincrease(5);
		currentspeed = currentspeed + speedincrease;
		System.out.println(currentspeed);

		//break method
		int speeddecrease = car.applybreak();
		currentspeed = currentspeed + speeddecrease;
		System.out.println(currentspeed);

	}

}
