package javaproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Switch {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		//Enter data using BufferReader 
		int n=2;
		//System.out.println("Enter input"); 
		//BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
		 //String name = reader.readLine();
		 //System.out.println(name); 
		 switch(n)
		 {
		 case 1:
			 System.out.println("Entered value is one");
			 break;
		 case 2:
			 System.out.println("Entered value is two");
			 break;
		 }
	}

}
