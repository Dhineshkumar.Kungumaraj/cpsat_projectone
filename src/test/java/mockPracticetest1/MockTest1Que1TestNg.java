package mockPracticetest1;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest1Que1TestNg {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
		driver.get("https://www.rediff.com/");
	}

	@Test
	public void RediffSet1() {
		By byvarnews = By.xpath("/html/body/div[5]/ul/li[2]/a");
		By byvarsubmenu = By.xpath("/html/body/div[5]/div[2]/ul/li[*]/a");
		By byvartooltip = By.xpath("/html/body/div[5]/div[1]/ul/li[3]/a");
		String Expbusinesstooltip = "Business headlines";
		String Expgranadiercolor = "#BC3E31";
		
		WebElement News = driver.findElement(byvarnews);
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/Screen1_News.png");
		News.click();
		System.out.println("Href of SubMenu:");
		List<WebElement> SubMenu = driver.findElements(byvarsubmenu);
		for(WebElement ele:SubMenu)
		{
		System.out.println(ele.getAttribute("href"));
		}

		WebElement BusinessTooltip = driver.findElement(byvartooltip);
		String ActtooltipText = BusinessTooltip.getAttribute("title");
		System.out.println(ActtooltipText);
		Assert.assertEquals(ActtooltipText, Expbusinesstooltip);
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HelperFunctions.captureScreenShot(driver, "src/test/resources/screenshots/Business_tooltip.png");
		
		String Background = driver.findElement(By.xpath("/html/body/div[5]/div[1]/ul/li[2]")).getCssValue("background-color"); 
		//System.out.println("Background color is : "+Background);

		String BG_RGB = Background.replace("rgba(", "rgb("); 
		String RGB =BG_RGB.substring(0, 15) + ")" ; 
		String RGB_Value = RGB.substring(3);
		//System.out.println("Background color in RGB is : "+ RGB ); 
		//System.out.println("RGB value is : "+ RGB_Value); 
		Assert.assertEquals("(188, 62, 49)", RGB_Value);
		
		
		String[] hexValue = Background.replace("rgba(", "").replace(")", "").split(",");                           
		hexValue[0] = hexValue[0].trim();
		int hexValue1 = Integer.parseInt(hexValue[0]);                   
		hexValue[1] = hexValue[1].trim();
		int hexValue2 = Integer.parseInt(hexValue[1]);                   
		hexValue[2] = hexValue[2].trim();
		int hexValue3 = Integer.parseInt(hexValue[2]);                   
		String Actgranadiercolor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3).toUpperCase();
		Assert.assertEquals(Actgranadiercolor, Expgranadiercolor);
		
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
