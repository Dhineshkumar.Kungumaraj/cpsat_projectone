package mockPracticetest1;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.DataWriter;

public class MockTest1Que4Junit {

	WebDriver driver;
	
	@Before
	public void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome", false);
		driver.get("https://www.rediff.com/");
	}

	@Test
	public void test() {
		String outputFileName = "src/test/resources/data/rediff.com.result.xlsx";
		String outputSheet = "Sheet1";
		
		By byvar1 = By.xpath("//*[@id=\"topdiv_0\"]/h2[*]/a");
		List<WebElement> wetopstories = driver.findElements(byvar1);
		for(WebElement ele:wetopstories)
		{
			System.out.println(ele.getText());
			ArrayList<String> list=new ArrayList<String>();//Creating arraylist    
			list.add(ele.getText());//Adding object in arraylist    

			try {
				DataWriter.writeToFile(outputFileName, outputSheet,list,true);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

	}
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}


}
