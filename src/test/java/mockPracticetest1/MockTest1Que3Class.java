package mockPracticetest1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.HelperFunctions;

public class MockTest1Que3Class {

	WebDriver driver;

	public MockTest1Que3Class(WebDriver driver) {
		this.driver = driver;
	}

	String Bseindexvalue = null;
	String Nseindexvalue = null;


	By byvarbse = By.xpath("//*[@id='bseindex']");
	By byvarnse= By.xpath("//*[@id='nseindex']");
	By byvarsearch = By.xpath("//*[@id=\"query\"]");
	By byvarsearchbtn = By.xpath("//*[@id=\"get_quote\"]/input[2]");

	String relianceindustriespagetitle()
	{
		String returnvalue = null;
		
		driver.switchTo().frame("moneyiframe");
		WebElement webseindex = driver.findElement(byvarbse);		
		Bseindexvalue = webseindex.getText();
		System.out.println(Bseindexvalue);
		
		WebElement wenseindex = driver.findElement(byvarnse);
		Nseindexvalue = wenseindex.getText();
		System.out.println(Nseindexvalue);
		
		WebElement wesearch = driver.findElement(byvarsearch);
		wesearch.sendKeys("Reliance Industries");
		
		WebElement wesearchbtn = driver.findElement(byvarsearchbtn);
		wesearchbtn.click();
		
		HelperFunctions.switchToWindow(driver, "RELIANCE INDUSTRIES LTD.");

		String Actualpagetitle1 = driver.getTitle();
		returnvalue = Actualpagetitle1;
		System.out.println(Actualpagetitle1);
		return returnvalue ;
	}
	String rediffpagetitle()
	{
		HelperFunctions.switchToWindow(driver, "Rediff.com: News");
		String returnvalue = null;
		String Actualpagetitle2 = driver.getTitle();
		returnvalue = Actualpagetitle2;
		System.out.println(Actualpagetitle2);
		return returnvalue ;

	}
}
