package mockPracticetest1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class MockTest1Que3POMJunit {
	
	WebDriver driver;

	@Before
	public void setUp()throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.rediff.com/");
		
	}
	@Test
	public void test() {
		
		String ExpRediffhomepagetitle = "Rediff.com: News | Rediffmail | Stock Quotes | Shopping";
		String ExpReliancepagetitle = "RELIANCE INDUSTRIES LTD. - Share Price | Ratios | BSE/NSE Performance | Live Stock Quote";
		
		MockTest1Que3Class object = new MockTest1Que3Class(driver);
		String ActReliancepagetitle = object.relianceindustriespagetitle();
		String ActRediffhomepagetitle = object.rediffpagetitle();
		Assert.assertEquals(ActRediffhomepagetitle, ExpRediffhomepagetitle);
		Assert.assertEquals(ActReliancepagetitle,ExpReliancepagetitle);

	}

	@After
	public void tearDown() throws Exception {
	driver.quit();
	}


}
