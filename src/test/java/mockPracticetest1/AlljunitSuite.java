package mockPracticetest1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MockTest1Que3POMJunit.class, MockTest1Que4Junit.class })
public class AlljunitSuite {

}
