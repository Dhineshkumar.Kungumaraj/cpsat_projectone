package mockPracticetest1;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class MockTest1Que2TestNgDD {
	WebDriver driver;
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
		driver.get("https://www.rediff.com/");
	}

	@Test
	(dataProvider = "dp")
	public void RediffSet2(String v1, String v2, String v3, String v4) throws InterruptedException 
	//public void RediffSet2() throws InterruptedException
	{
		String ExpVarMenu = v1.toUpperCase();
		//String ExpVarMenu = "NEWS";
		String ExpVarSubmenu = v2.toUpperCase();
		String ExpVarHref = v3;
		String Screenshotpath ="src/test/resources/screenshots/";
		String index = v4;
		String fileextension =".png";

		String ActVarMenu = null;
		String ActVarSubmenu = null;
		String ActVarHref = null;


		WebElement byvarnews = driver.findElement(By.xpath("/html/body/div[5]/ul/li[2]/a"));
		ActVarMenu = byvarnews.getText();
		Assert.assertEquals(ActVarMenu, ExpVarMenu);
		if(byvarnews.isDisplayed()) {	
			
			byvarnews.click();
		}
		else
		{
			HelperFunctions.switchToWindow(driver,"Latest India News, Headlines, Stories and Videos");
			byvarnews.click();
		}

		By byvarsubmenu = By.xpath("/html/body/div[5]/div[2]/ul/li["+index+"]/a");
		WebElement Submenu = driver.findElement(byvarsubmenu);
		List<WebElement> lstNewmenu = driver.findElements(byvarsubmenu);
		for (int i = 1; i<=lstNewmenu.size(); i++) {
			for( WebElement ele:lstNewmenu)
			{
				if(ele.getText().contains(ExpVarSubmenu))
				{
					ActVarSubmenu = ele.getText();
					Assert.assertEquals(ActVarSubmenu, ExpVarSubmenu);
					ActVarHref = ele.getAttribute("href");
					Assert.assertEquals(ActVarHref, ExpVarHref);
				}
			}
			Actions action = new Actions(driver);
			action.moveToElement(Submenu);
			action.keyDown(Keys.CONTROL);
			action.click();
			action.keyDown(Keys.CONTROL).build().perform();
			HelperFunctions.switchToWindow(driver,ExpVarSubmenu);
			Thread.sleep(5000);
			HelperFunctions.captureScreenShot(driver,Screenshotpath+"Submenu"+index+fileextension);
			Thread.sleep(5000);
			HelperFunctions.switchToWindow(driver,"Latest India News, Headlines, Stories and Videos");
			driver.navigate().back();
		}
	}



	@DataProvider 
	public Object[][] dp() throws EncryptedDocumentException,	InvalidFormatException { 
		Object[][] data=null; 
		try { data =utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\Rediff.excel.xlsx", "sheet1");
		}
		catch(IOException e) 
		{ e.printStackTrace(); 
		}
		return data; 
	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
